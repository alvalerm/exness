# Description: estimate the distribution of the update times
# Date: 2018-06-18
# Author: Alexey  

# align multiple series  ----------------------------------------------------------

load("./data/R/01_alignSeries.R")
load("./data/R/00_sourceData.rda")

getUpdateTime <- function(df) {
  update_times <- select(df, time_unix) %>% 
    mutate(Week = lubridate::week(time_unix),
           WeekDay = lubridate::wday(time_unix),
           Week = if_else(WeekDay == 1, Week + 1, Week)) %>% 
    group_by(Week) %>% 
    mutate(UpdateTime = time_unix - dplyr::lag(time_unix, 1, order_by = time_unix),
           UpdateTimeNumeric = as.numeric(UpdateTime)) %>% 
    ungroup() %>% 
    select(time_unix, UpdateTimeNumeric)  
}

update_times <- data_lastTick %>% 
  group_by(name) %>% 
  tidyr::nest() %>% 
  mutate(dist = purrr::map(data, getUpdateTime)) %>% 
  select(name, dist) %>% 
  tidyr::unnest()

update_times_all <- getUpdateTime(data_cleaned) %>% 
  mutate(name = "All") %>% bind_rows(update_times)

# plot update times distribution ------------------------------------------

update_times_all %>% 
  ggplot(aes(UpdateTimeNumeric, col = name, fill = name)) +
  geom_histogram(aes(y = (..count..)/sum(..count..))) +
  xlim(0, 20) +
  facet_wrap(~name, scales = "free_y") +
  ylab("Density") +
  xlab("Update time") +
  ggtitle("Distribution of update times") +
  theme(legend.position = "none")

ggsave(filename = "./figures/10_updateDistribution.jpg")


# save the results --------------------------------------------------------


save(update_times_all, update_times,
     file = "./data/R/10_updateDistribution.rda")



# clean up ----------------------------------------------------------------

rm(list = ls())
