SharpeConfidenceBounds <- function(profits,
                                   freq          = 252,
                                   use_frequency = FALSE,
                                   alpha         = 0.05) {
  stopifnot(is.numeric(profits))
  stopifnot(length(profits) >= 2)
  freq <- ifelse(use_frequency, freq, 1)
  sharpe_estimate <- SharpeEstimate(profits)
  sharpe_std      <- SharpeStd(profits)
  q_low           <- qnorm(alpha / 2)
  q_high          <- qnorm(1 - alpha / 2)
  mult            <- sqrt(freq)
  data.frame(
    Sharpe   = sharpe_estimate * mult,
    LowBound = (sharpe_estimate + q_low * sharpe_std) * mult,
    UpBound  = (sharpe_estimate + q_high * sharpe_std) * mult
  )
}

SharpeEstimate <- function(profits) {
  stopifnot(is.numeric(profits))
  stopifnot(length(profits) >= 2)
  sharpe <- mean(profits, na.rm = TRUE)/sd(profits, na.rm = TRUE)
}

SharpeStd <- function(profits) {
  library(moments)
  N          <- length(profits)
  s_estimate <- SharpeEstimate(profits)
  s          <- sd(profits, na.rm = TRUE)
  m_3        <- moment(profits, order = 3, central = TRUE)
  m_4        <- moment(profits, order = 4, central = TRUE)
  
  s_std <-
    sqrt((1 + (s_estimate ^ 2) / 4 * (m_4 / s ^ 4 - 1) - s_estimate * m_3 /
            s ^ 3) / (N - 1))
}

plotProfits <- function(profits_data, title = "Equity of the strategy") {
  stopifnot(c("Profit", "Date") %in% colnames(profits_data))
  library(ggplot2)
  library(dplyr)
  library(reshape2)
  library(gridExtra)
  sharpe <- SharpeConfidenceBounds(profits_data$Profit) %>% 
    mutate_all(round, digits = 2) %>% 
    melt(measure.vars = c(1,2,3))
  eq <- profits_data %>% 
    mutate(Equity = cumsum(Profit)) 
  xmin = as.Date("2018-05-11")
  xmax = as.Date("2018-05-13")
  ymin = ifelse(max(eq$Equity) > 0, 7*max(eq$Equity)/10, max(eq$Equity))
  ymax = ifelse(max(eq$Equity) > 0, 9*max(eq$Equity)/10, max(eq$Equity)/4)
  ggplot(eq, aes(Date, Equity)) +
    geom_line() +
    annotation_custom(tableGrob(sharpe,rows = NULL, cols = NULL, theme = ttheme_minimal()), 
                    #  xmin = xmin, 
                    #  xmax = xmax, 
                      ymin = ymin, 
                      ymax = ymax) +
    ggtitle(title)
  
}